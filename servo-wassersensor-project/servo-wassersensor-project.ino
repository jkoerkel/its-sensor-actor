#include <Servo.h>
#include <LiquidCrystal.h>


int measuredValue = 0; //Unter der Variablen "messwert" wird später der Messwert des Sensors gespeichert.
int oldMeasuredValue = 0;

// DC Motor
const int inputPin1  = 8;
const int inputPin2  = 9;

// LCD Display
LiquidCrystal lcd(2, 3, 4, 5, 6 , 7);

void setup()
{
  Serial.begin(9600);
  pinMode(inputPin1, OUTPUT);
  pinMode(inputPin2, OUTPUT);
  lcd.begin(16, 2);
  lcd.setCursor(0,0);
  lcd.print("Feuchtigkeit: ");
}

void loop()
{
  if (isWet())
  {
    digitalWrite(inputPin1, HIGH);
    digitalWrite(inputPin2, LOW);
  } else {
    digitalWrite(inputPin1, LOW);
  }

  clearLine();
  lcd.setCursor(0,1);
  lcd.print((String)measuredValue);
}

bool isWet() 
{
  measuredValue = analogRead(A0);
  bool result = false;

  Serial.print("Feuchtigkeits-Messwert:"); 
  Serial.println(measuredValue);
  delay(250);

  if (measuredValue > 0 && oldMeasuredValue > 0)
  {
    result = true;
  }
  oldMeasuredValue = measuredValue;

  return result;
}

void clearLine()
{
  for(int i = 0; i < 16; i++) {
    lcd.setCursor(i,1);
    lcd.print(" "); 
  }
}
